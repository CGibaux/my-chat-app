<?php

namespace App\Controller;

use App\Entity\MessageState;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Message;
use App\Entity\Groupe;

use App\Form\MessageType;

class MessageController extends AbstractController
{

    /**
    * @Route("/messagerie", name="messagerie")
    */
        public function viewMessage(Request $request){
            
            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

            $manager = $this -> getDoctrine() -> getManager();
            $user = $this -> getUser();
            $groupes = $manager -> getRepository(Groupe::class) -> findGroupByUser($user);
            $tableauGroupes = [];
            $i = 0;
            foreach($groupes as $groupe){
                $tableauGroupes[$i] = $groupe -> getId();
                $i++;
            };
            
            $tableauMessages = [];
            $i = 0;
            foreach($tableauGroupes as $groupe){
                $message = $manager -> getRepository(Message::class) -> findMessageByGroupe($groupe);
                $groupeName = $manager -> getRepository(Groupe::class) -> findOneById($groupe);
                $groupeName = $groupeName -> getNom();
                $tableauMessages[$groupeName] = $message;
                $i++;
            };
            $messages = $tableauMessages;
            //formulaire...
            $message = new Message;
            $form = $this -> createForm(MessageType::class, $message);


            // traitement des infos du formulaire
            $form -> handleRequest($request); //lier définitivement les $message aux infos du formulaire (récupère les données en $\MESSAGE)

            if ($form -> isSubmitted() && $form -> isValid()) {
                $manager -> persist($message); // D'enregistrer le message dans le system


                $message -> setDateTime(new \DateTime('now'));
                $message -> setUser($this -> getUser());
                $messageState = $manager -> getRepository(MessageState::class) -> findOneById(2);
                $message -> setMessageState($messageState);


                $manager -> flush();
                return $this -> redirectToRoute('messagerie');
            }

            return $this -> render('message/messagerie.html.twig',array(
                'messageForm' => $form -> createView(),
                'messages' => $messages
            ));
        }
}
