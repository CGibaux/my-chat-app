<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


use App\Entity\User;
use App\Entity\Groupe;
use App\Entity\Message;
use App\Form\UserType;
use App\Form\GroupeType;
use App\Form\MessageType;

class UserController extends AbstractController
{
    /**
     * @Route("/signup", name="signup")
     * localhost:8000/signup
     */
    public function signup(Request $request, UserPasswordEncoderInterface $encoder) {

          $manager = $this -> getDoctrine() -> getManager();
          $user = new User;
          $form = $this -> createForm(UserType::class, $user);
          $form -> handleRequest($request);
          if($form -> isSubmitted() && $form -> isValid()){
              $manager -> persist($user);
              $password = $user -> getPassword();
              $user -> setPassword($encoder -> encodePassword($user, $password));
              $manager -> flush();
              $this -> addFlash('success', 'Votre inscription a été prise en compte');
              return $this -> redirectToRoute('accueil');
          }

        return $this -> render('user/signup.html.twig', array(
                    'userForm' => $form -> createView()
                ));
    }

    /**
    * @Route("/", name="accueil")
    * localhost:8000/
    */
    public function signin(AuthenticationUtils $auth){
        $lastUsername = $auth -> getLastUsername();
        $error = $auth -> getLastAuthenticationError();


        return $this->render('user/login.html.twig', array(
                    'lastUsername' => $lastUsername,
                    'error' => $error
                ));
    }

    /**
    * Route necessaire pour le fonctionnement de sécurité de ma connexion de SF
    * @Route("/login_check", name="login_check")
    */
    public function loginCheck(){

    }

    /**
    * @Route("/logout", name="logout")
    * localhost:8000/logout
    */
    public function logout(){
        return $this->redirectToRoute('accueil');
    }


    /**
    * @Route("/groupe/add", name="groupe_add")
    *
    */
    public function creationGroupe(Request $request){
            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
            $manager = $this -> getDoctrine() -> getManager();
            $groupe = new Groupe;
            $user = new User;
            $users_p = new User;

            $form = $this -> createForm(GroupeType::class, $groupe);
            $form -> handleRequest($request);

            if($form -> isSubmitted() && $form -> isValid()){
                $manager -> persist($groupe);
                $groupe -> setDate(new \DateTime('now'));
                $groupe -> setUsersP($this -> getUser());

                //$groupe -> getUsers() --> array Collection
                foreach($groupe -> getUsers() as $user){
                    //$groupe -> addUser($user);
                    $user -> addGroupe($groupe);
                }
                $manager -> flush();
            }
            return $this -> render('groupe/groupadd.html.twig',array(
                'groupForm' => $form -> createView()));
        }
}