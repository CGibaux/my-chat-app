<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Groupe;
use App\Entity\User;
use App\Form\GroupeType;

class GroupeController extends AbstractController
{
    /**
     * @Route("/groupe", name="groupe")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('groupe/index.html.twig', [
            'controller_name' => 'GroupeController',
        ]);
    }

    /**
     * @Route("/show/{id}", name="show")
     */
    public function show($id) {
        
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        //1 : Récupérer les info du groupe
        $repo = $this -> getDoctrine() -> getRepository(Groupe::class);
        $groupes = $repo -> find($id);

        //2 : Afficher la vue avec les infos
        return $this -> render("groupe/show.html.twig", array(
            'groupe' => $groupes
        ));
    }

    /**
     * @Route("/groupe/delete/{id}", name="groupe_delete")
     */
    public function GroupeDelete($id){
        
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        //1: Manager
        $manager = $this -> getDoctrine() -> getManager();
        //2 : Récupérer l'entrée à suppr
        $groupe = $manager ->find(Groupe::class, $id);
        //3 : Suppr
        $manager -> remove($groupe);
        $manager -> flush();
        //4 : Message
        $this -> addFlash('success', 'Le groupe N°' . $id . ' a bien été supprimé !');
        //5 : redirection
        return $this -> redirectToRoute('groupe_delete');
    }



    /**
     * @Route("/groupe/update/{id}", name="groupe_update")
     */
    public function GroupeUpdate($id, Request $request){
        
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        //1: récupérer le manager
        $manager = $this -> getDoctrine() -> getManager();
        //2 : Récupérer l'objet
        $groupe = $manager -> find(Groupe::class, $id);
        $form = $this -> createForm(GroupeType::class, $groupe);
        // Notre objet hydrate le formulaire

        $form -> handleRequest($request);

        if($form -> isSubmitted() && $form -> isValid()) {

            //3 : Modifier (formulaire)
            $manager -> persist($groupe);

            if($groupe -> getFile()) {
                $groupe -> removeFile();
                $groupe -> uploadFile();
            }

            $manager -> flush();
            //4 : Message
            $this -> addFlash('success', 'Le Groupe N°' . $id . 'a bien été modifié !');
            return $this -> redirectToRoute('groupe_update');

        }

        //5 : vue
        return $this -> render('groupe/groupeForm.html.twig', [
            'groupeForm' => $form -> createView()
            ]);
    }

}
