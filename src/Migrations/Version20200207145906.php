<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200207145906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C21A9FA2F6B');
        $this->addSql('DROP INDEX IDX_4B98C21A9FA2F6B ON groupe');
        $this->addSql('ALTER TABLE groupe CHANGE user_p_id users_p_id INT NOT NULL, CHANGE name nom VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C211CF22DAC FOREIGN KEY (users_p_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_4B98C211CF22DAC ON groupe (users_p_id)');
        $this->addSql('ALTER TABLE message CHANGE datetime date_time DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user ADD role VARCHAR(20) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE groupe DROP FOREIGN KEY FK_4B98C211CF22DAC');
        $this->addSql('DROP INDEX IDX_4B98C211CF22DAC ON groupe');
        $this->addSql('ALTER TABLE groupe CHANGE users_p_id user_p_id INT NOT NULL, CHANGE nom name VARCHAR(50) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE groupe ADD CONSTRAINT FK_4B98C21A9FA2F6B FOREIGN KEY (user_p_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_4B98C21A9FA2F6B ON groupe (user_p_id)');
        $this->addSql('ALTER TABLE message CHANGE date_time datetime DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user DROP role');
    }
}
